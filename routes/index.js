var express = require('express');
var router = express.Router();
const Multer = require ('multer');
const datastore = require('../lib/dataStoreLayer.js');
const storage = require('../lib/storageAccessLayer');
var oauth2 = require('../lib/oauth2');
var multer = Multer({
  storage: Multer.MemoryStorage,
  limits: {
    fileSize: 8 * 1024 * 1024
  }
})

router.use(oauth2.template);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.redirect('/static/homepage.html');
});

router.get("/login", oauth2.required, function(req, res, next){
  res.redirect('/list');
});

router.get("/myName", oauth2.required, function(req, res, next){
  console.log("Your Name:", req.user.displayName);
  res.sendStatus(200);
});

router.get('/myList', oauth2.required, function(req, res, next){
  datastore.listPersonalSpace(req.user.id, 10, null, function(err, dataSet){
    if(!err){
      translateAndRender(dataSet, res);
    } else {
      res.sendStatus(500);
    }
  });
});

function translateAndRender(dataSet, res){
  var renderList = [];
  console.log(dataSet);
  dataSet.map(function(elem){
    var reducedVar = {};
    reducedVar["imgUrl"] = elem.imageurl;
    reducedVar["place"] = elem.place;
    reducedVar["caption"] = elem.caption;
    renderList.push(reducedVar);
  });
  console.log(renderList);
  var renderObj = {pictures: renderList};
  res.render('listPics', renderObj);  
}
/* List Pictures */
router.get('/list', function(req, res, next){
  console.log("List request by :", req.user);
  datastore.list(100, null, function(err, dataSet, token){
    if (err){
      console.log("Error in getting the list from data store:", err);
    } else {
      console.log("Received dataset :", JSON.stringify(dataSet));
      var renderObj = {
        pictures:[]
    };
      dataSet.map(function(elem){
        var reducedVar = {};
        reducedVar["imgUrl"] = elem.imageurl;
        reducedVar["place"] = elem.place;
        reducedVar["caption"] = elem.caption;
        renderObj.pictures.push(reducedVar);
      });
      console.log("Rendering objs:", JSON.stringify(renderObj));
      res.render('listPics', renderObj);

    }
  });
});

/*  Add Picture */
router.get('/add', oauth2.required ,function(req, res, next){
  res.render('AddPhoto', {action: "Add Picture", picture: {}});
});

router.post('/add', storage.multer.single('image'), storage.sendUploadToGCS, function(req, res, next){
  console.log("body:", req.body);
  console.log("fileDetails:", req.file);
  var fileMetaData = {};
  fileMetaData["caption"] = req.body.title.toUpperCase();
  fileMetaData["place"] = req.body.place.toUpperCase();
  fileMetaData["tags"] = req.body.tags.split(',').map(x=>x.trim().toUpperCase());
  fileMetaData["publish"] = req.body.publish;
  fileMetaData["originalname"] = req.file.originalname;
  fileMetaData["imageurl"] = req.file.cloudStoragePublicUrl;
  fileMetaData["userid"] = req.user.id;
  console.log(JSON.stringify(fileMetaData));
  datastore.create(fileMetaData, function(err){

    if (err){
      console.log(err);
      res.sendStatus(500);
    } else {
        console.log("Added to Data store");
        res.redirect('/list');
    }
  });
});

router.get('/search', function(req, res, next){
  console.log("Search for:", req.query);
  var query = req.query.search;
  console.log(query);
  var key, value;
  var pos = query.indexOf(":");
  if ( -1 == pos ) {
    key = "tags";
    value = query.toUpperCase();
  } else {
    key = query.substring(0, pos).toLowerCase();
    value = query.substring(pos + 1, query.length).toUpperCase();
  }
  console.log("Key:", key, " Value:", value);
  datastore.search(key, value, 10, null, function(err, dataSet){
    var renderList = [];
    console.log(dataSet);
    dataSet.map(function(elem){
      var reducedVar = {};
      reducedVar["imgUrl"] = elem.imageurl;
      reducedVar["place"] = elem.place;
      reducedVar["caption"] = elem.caption;
      renderList.push(reducedVar);
    });
    console.log(renderList);
    var renderObj = {pictures: renderList};
    res.render('listPics', renderObj);
  })
  
});
module.exports = router;
